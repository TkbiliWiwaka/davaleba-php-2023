<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>სახელი გვარი </title>
</head>
<body>

<form action="process_form.php" method="get">
    <label for="first_name">სახელი:</label>
    <input type="text" name="first_name" required><br>

    <label for="last_name">გვარი:</label>
    <input type="text" name="last_name" required><br>

    <label for="position">პოზიცია:</label>
    <input type="text" name="position" required><br>

    <label for="amount">ღირებულებია:</label>
    <input type="text" name="amount" required><br>

    <label for="income_type">შემოსავალი:</label>
    <select name="income_type">
        <option value="percentage">პროცენტი</option>
        <option value="figure">ფიგურა</option>
    </select><br>

    <button type="submit">დათანხმება</button>
</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $first_name = $_GET["first_name"];
    $last_name = $_GET["last_name"];
    $position = $_GET["position"];
    $amount = $_GET["amount"];
    $income_type = $_GET["income_type"];

    if ($income_type === "percentage") {
        $other_income = 0.2 * $amount;
        $accrued_amount = 0.8 * $amount;
    } else {

    }

    echo "First Name: $first_name<br>";
    echo "Last Name: $last_name<br>";
    echo "Position Held: $position<br>";
    echo "Amount: $amount<br>";
    echo "Other Income: $other_income<br>";
    echo "Accrued Amount: $accrued_amount<br>";
}
?>
</body>
</html>

