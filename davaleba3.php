<?php
function calculateFactorial($n) {
    if ($n == 0 || $n == 1) {
        return 1;
    } else {
        return $n * calculateFactorial($n - 1);
    }
}
$number = 30;
echo "Factorial of $number is: " . calculateFactorial($number);
?>
<br>
<br>
<br>
<?php
function isPrime($num) {
    if ($num <= 1) {
        return false;
    }
    for ($i = 2; $i <= sqrt($num); $i++) {
        if ($num % $i == 0) {
            return false;
        }
    }
    return true;
}
$number = 17;
if (isPrime($number)) {
    echo "$number არის მარტივი რიცხვი";
} else {
    echo "$number არ არის  მარტივი რიცხვი";
}
?>
<br>
<br>
<br>
<?php
function reverseString($str) {
    return strrev($str);
}
$string = "Hello, World!";
echo "შებრუნებული სტრიქონი:" . reverseString($string);
?>
<br>
<br>
<br>
<?php
function sortArray($arr) {
    sort($arr);
    return $arr;
}
$array = [3, 1, 4, 1, 5, 9, 2];
echo "დახარისხებული მასივი: " . implode(", ", sortArray($array));

?>
<br>
<br>
<br>
<?php
function isLowerCase($str) {
    return $str === strtolower($str);
}
$string = "გამარჯობა";
if (isLowerCase($string)) {
    echo "სტრიქონი მთლიანად პატარაა.";
} else {
    echo "სტრიქონი შეიცავს დიდ ასოებს.";
}
?>
<br>
<br>
<br>
<?php
function isPalindrome($str) {
    $str = strtolower(str_replace(' ', '', $str));
    return $str === strrev($str);
}
$phrase = "კაცი გეგმავს";
if (isPalindrome($phrase)) {
    echo "ფრაზა პალინდრომია.";
} else {
    echo "ფრაზა არ არის პალინდრომი.";
}
?>
<br>
<br>
<br>
