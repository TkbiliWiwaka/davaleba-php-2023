<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>მასივი</title>
</head>
<body>

<form action="" method="post">
    <label for="number_x">დამატება X:</label>
    <input type="number" name="number_x" required>
    <button type="submit">გაუშვი</button>
</form>

<?php
$numeric_array = array_fill(0, 12, 0);
for ($i = 5; $i < 8; $i++) {
    $numeric_array[$i] = rand(10, 100);
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $x_value = $_POST["number_x"];
    $count_greater_than_x = 0;
    foreach ($numeric_array as $element) {
        if ($element >= $x_value) {
            $count_greater_than_x++;
        }
    }
    echo "<p>მასივი: " . implode(", ", $numeric_array) . "</p>";
    echo "<p>X-ზე მეტი ან ტოლი ელემენტების რაოდენობა ($x_value): $count_greater_than_x</p>";
}
?>

</body>
</html>
