<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>შემთხვევითი Tables</title>
    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid red;
            padding: 10px;
            text-align: center;
        }
    </style>
</head>
<body>
<?php
function generateRandomTable() {
    echo "<table>";
    for ($row = 0; $row < 10; $row++) {
        echo "<tr>";
        for ($col = 0; $col < 10; $col++) {
            $random_number = rand(10, 99);
            echo "<td>$random_number</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}
generateRandomTable();
?>

</body>
</html>
